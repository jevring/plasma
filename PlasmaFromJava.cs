using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace Plasma
{
	/// <summary>
	/// Summary description for PlasmaFromJava.
	/// </summary>
	public class PlasmaFromJava
	{
		int width, height;
		PictureBox image;
		Bitmap bmp;
		PlasmaLockBits.PixelData [] colors = new PlasmaLockBits.PixelData[256];
		int [] plasma1 = new int[512*512];
		int [] plasma2 = new int[512*512];
		bool running = true;

		int px=256;
		int py=256;
		int p2x=45;
		int p2y=256;
		int l2x;
		int l2y;
		int lx;
		int ly;
		int ll=0;

		public PlasmaFromJava(int width, int height, PictureBox image)
		{
			this.width = width;
			this.height = height;
			this.image = image;
			bmp = new Bitmap(width, height);

			int cr = 0;
			int cg = 0;
			int cb = 255;
			
			for (int i = 0; i < 256; i++)
			{
				// R + G + B
				PlasmaLockBits.PixelData pixel = new PlasmaLockBits.PixelData(Convert.ToByte(cr),Convert.ToByte(cg),Convert.ToByte(cb));
				colors[i] = pixel;
				
				if (cb < 128)
				{
					cb--;
				}
				else
				{
					cb -= 2;
				}
				if (cb < 0) cb = 0;
				if (cb > 200)
				{
					cg++;
					cr++;
				}
				if (cr > 64) cg++;
				if (cb < 128) cr++;
				//Console.Out.WriteLine(cr + ";" + cg + ";" + cb);
			}
			

			int dst = 0;
			for (int i = 0; i < 512; i++)
			{
				for (int j = 0; j < 512; j++)
				{
//					           plasma1[dst] = (int)( 64 + 63 *(Math.Sin((double)Math.Sqrt((double)((256-j)*(256-j)+(256-i)*(256-i))) /16 )));
//					           plasma2[dst] = (int)( 64 + 63 *(Math.Sin((double)Math.Sqrt((double)((128-j)*(128-j)+(200-i)*(200-i))) /57 )));
					plasma1[dst] = (int)( 64 + 63 * Math.Sin((double)i/(38+14*Math.Cos((double)j/70)) )* Math.Cos((double)j/(33+15*Math.Sin((double)i/60))) );
					plasma2[dst] = (int)( 64 + 63 * Math.Sin((double)i/(37+15*Math.Cos((double)j/74)) )* Math.Cos((double)j/(31+11*Math.Sin((double)i/57))) );
					dst++;
				}
			}

		}

		public void Run()
		{


			int color = 0;
			
			while (running)
			{
				lx=128+(int)(120*Math.Sin((px*0.01745)));
				l2x=128+(int)(120*Math.Sin((p2x*0.01745)));
				ly=128+(int)(120*Math.Cos((px*0.01745)));
				l2y=128+(int)(120*Math.Cos((p2x*0.01745)));

				BitmapData bmpBits = bmp.LockBits(new Rectangle(0,0,width,height), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
				IntPtr pBase = bmpBits.Scan0;
				for (int x = 0; x < width; x++)
				{
					for (int y = 0; y < height; y++)
					{
						color = PlasmaFunction(x,y);
						//bmp.SetPixel(x,y,Color.FromArgb(colors[color].red,  colors[color].green,  colors[color].blue));
						unsafe 
						{
							PlasmaLockBits.PixelData* pixel = PlasmaLockBits.PixelAt(x, y, pBase, width);
							pixel->red = colors[color].red;
							pixel->green = colors[color].green;
							pixel->blue = colors[color].blue;
						}
				
					}
				}
				bmp.UnlockBits(bmpBits);
				image.Image = bmp;
				image.Update();

				p2x--; 
				if (p2x<-360) p2x=0;
				if(ll==0) px++; 
				if (px>360) {px=0;}
				else {ll++;}
				if(ll>1) ll=0;

			}
		}
		public int PlasmaFunction(int x, int y)
		{
			return plasma1[(lx+x)+(y+ly)*512]+plasma2[(x+l2x)+(y+l2y)*512];
		}
	}
}
