using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace Plasma
{
	/// <summary>
	/// This class uses the sinus array trick, and uses unsafe code (bmp.LockBits()) to update the double buffered image.
	/// The image is then bitblited onto the screen (via assignment, the .net framework does the behind-the-scenes blitting) 
	/// </summary>
	public class PlasmaLockBits
	{
		double [] sinArray = new double[360];
		int inc = 0;
		int width;
		int height;
		Bitmap bmp;
		PictureBox image;
		bool running = true;
		
		public PlasmaLockBits(int width, int height, PictureBox image)
		{
			this.width = width;
			this.height = height;
			this.image = image;
			// build sinus array (360 degree period)
			for (int i = 0; i < 360; i++)
			{
				sinArray[i] = Math.Sin(Plasma.ToRad(i));
				//Console.Out.WriteLine(i + "=" + sinArray[i]);
			}
			bmp = new Bitmap(width, height);
		}

		public struct PixelData
		{
			public PixelData(byte red, byte green, byte blue)
			{
				this.red = red;
				this.green = green;
				this.blue = blue;
			}

			public byte red;
			public byte green;
			public byte blue;
		}
		public static unsafe PixelData* PixelAt(int x, int y, IntPtr pBase, int width)
		{
			return (PixelData*) ((long)pBase + y * (width * sizeof(PixelData)) + x * sizeof(PixelData));	
		}


		public Color NormalizedColorize(int c, int range)
		{
			// the 'range' goes from -range to +range, and we want a value within that range
			// normalized into a new range between 0 to 2048
			double n = (c + range); // move the potential bottom up from -range to 0
			n = n / (2 * range);  // create the smallest part (should be between 0 and 1)
			n = n * 2047;
				
			int i = (int)n;
			if (i >= 0 && i < 256)
			{
				return Color.FromArgb(0,0,i);
			}
			else if (i >= 256 && i < 512)
			{
				return Color.FromArgb(0, (i - 256), 255);
			}
			else if (i >= 512 && i < 768)
			{
				return Color.FromArgb((i - 512), 255, 0);
			}
			else if (i >= 768 && i < 1024)
			{
				return Color.FromArgb((i - 768), 255, 255);
			}
			else if (i >= 1024 && i < 1280)
			{
				return Color.FromArgb((i - 1024),0,0);
			}
			else if (i >= 1280 && i < 1536)
			{
				return Color.FromArgb(255, (i - 1280), 0);
			}
			else if (i >= 1536 && i < 1792)
			{
				return Color.FromArgb(0, 255, (i - 1536));
			}
			else if (i >= 1792 && i < 2048)
			{
				return Color.FromArgb(255, 255, (i - 1792));
			}
			/*
			00i
			0i0
			i00
			i0f
			if0
			iff
			i00
			0i0
			00i
			f0i
			0fi
			ffi
			*/

			Console.Out.WriteLine("c=" + c + ";n=" + n);
			return Color.Black;
		}

		public void Run()
		{
			double xValue;
			double yValue;
			while(running)
			{
				long start = System.DateTime.Now.Ticks;
				inc += 2;
				//inc += 10;
				BitmapData bmpBits = bmp.LockBits(new Rectangle(0,0,width, height),ImageLockMode.WriteOnly,PixelFormat.Format24bppRgb);
				IntPtr pBase = bmpBits.Scan0;
				for (int x = 0; x < width; x++)
				{
					xValue =	//40 * Math.Cos((double)(x + inc) / 100) + 60 * Math.Cos((double)(x + inc) / 100) + 10 * Math.Cos((double)x / 100);
								40 * sinArray[((x * 4) + inc) % 360] + 
								30 * sinArray[(x + inc * 4) % 360] + 
								20 * sinArray[((x / 4) + (inc / 4)) % 360];
					
					for (int y = 0; y < height; y++)
					{
						yValue =	//40 * Math.Sin((double)(y + inc) / 400) + 60 * Math.Cos((double)(y+inc) / 100) + 10 * Math.Cos((double)(y - inc) / 100);
									40 * sinArray[((y * 6) + inc) % 360] + 
									34 * sinArray[(y + inc * 6) % 360] + 
									25 * sinArray[(y + (inc / 6)) % 360];
						int c = Convert.ToInt32(xValue + yValue);
						//NormalizedColorize(c, 189);
						
						unsafe 
						{
							
							Color col = NormalizedColorize(c, 189);
							PixelData* pixel = PixelAt(x, y, pBase, width);
							pixel->red = col.R;
							pixel->green = col.G;
							pixel->blue = col.B;
						}
						
					}
				}
				bmp.UnlockBits(bmpBits);
				image.Image = bmp;
				image.Update();
				long timeToRenderOneFrame = System.DateTime.Now.Ticks - start;
				//this.Text = "fps: " + 1 / (timeToRenderOneFrame * 0.0000001);

			}
		}

	}
}
