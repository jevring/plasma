using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Windows.Forms;

namespace Plasma
{
	/// <summary>
	/// Summary description for Plasma.
	/// </summary>
	public class Plasma : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button plasmaSimple;
		private System.Windows.Forms.Button plasmaArray;
		private System.Windows.Forms.Button plasmaPreBuffer;
		private System.Windows.Forms.Button playPlasmaBuffer;
		private System.Windows.Forms.NumericUpDown sleepTimer;
		
		private System.Windows.Forms.Button plasmaColorCycle;
		private System.Windows.Forms.Button plasmaArrayLockBits;
		private System.Windows.Forms.PictureBox image;
		private System.Windows.Forms.Button start;
		private System.Windows.Forms.Button stop;
		private bool running = true;
		private static int width = 256;
		private static int height = 256;
		private System.Windows.Forms.Button plasmaFromJava;
		private Thread runningThread = null;
		
		public Plasma()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			image.Size = new Size(width, height);
			
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.image = new System.Windows.Forms.PictureBox();
			this.plasmaSimple = new System.Windows.Forms.Button();
			this.start = new System.Windows.Forms.Button();
			this.stop = new System.Windows.Forms.Button();
			this.plasmaArray = new System.Windows.Forms.Button();
			this.plasmaPreBuffer = new System.Windows.Forms.Button();
			this.playPlasmaBuffer = new System.Windows.Forms.Button();
			this.sleepTimer = new System.Windows.Forms.NumericUpDown();
			this.plasmaColorCycle = new System.Windows.Forms.Button();
			this.plasmaArrayLockBits = new System.Windows.Forms.Button();
			this.plasmaFromJava = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.sleepTimer)).BeginInit();
			this.SuspendLayout();
			// 
			// image
			// 
			this.image.Location = new System.Drawing.Point(8, 8);
			this.image.Name = "image";
			this.image.Size = new System.Drawing.Size(256, 256);
			this.image.TabIndex = 0;
			this.image.TabStop = false;
			// 
			// plasmaSimple
			// 
			this.plasmaSimple.Location = new System.Drawing.Point(8, 568);
			this.plasmaSimple.Name = "plasmaSimple";
			this.plasmaSimple.Size = new System.Drawing.Size(96, 23);
			this.plasmaSimple.TabIndex = 1;
			this.plasmaSimple.Text = "plasma_simple";
			this.plasmaSimple.Click += new System.EventHandler(this.plasmaSimple_Click);
			// 
			// start
			// 
			this.start.Location = new System.Drawing.Point(376, 632);
			this.start.Name = "start";
			this.start.TabIndex = 2;
			this.start.Text = "start";
			this.start.Click += new System.EventHandler(this.start_Click);
			// 
			// stop
			// 
			this.stop.Location = new System.Drawing.Point(288, 632);
			this.stop.Name = "stop";
			this.stop.TabIndex = 2;
			this.stop.Text = "stop";
			this.stop.Click += new System.EventHandler(this.stop_Click);
			// 
			// plasmaArray
			// 
			this.plasmaArray.Location = new System.Drawing.Point(8, 600);
			this.plasmaArray.Name = "plasmaArray";
			this.plasmaArray.Size = new System.Drawing.Size(96, 23);
			this.plasmaArray.TabIndex = 1;
			this.plasmaArray.Text = "plasma_array";
			this.plasmaArray.Click += new System.EventHandler(this.plasmaArray_Click);
			// 
			// plasmaPreBuffer
			// 
			this.plasmaPreBuffer.Location = new System.Drawing.Point(128, 568);
			this.plasmaPreBuffer.Name = "plasmaPreBuffer";
			this.plasmaPreBuffer.Size = new System.Drawing.Size(112, 23);
			this.plasmaPreBuffer.TabIndex = 1;
			this.plasmaPreBuffer.Text = "plasma_prebuffer";
			this.plasmaPreBuffer.Click += new System.EventHandler(this.plasmaPreBuffer_Click);
			// 
			// playPlasmaBuffer
			// 
			this.playPlasmaBuffer.Location = new System.Drawing.Point(248, 568);
			this.playPlasmaBuffer.Name = "playPlasmaBuffer";
			this.playPlasmaBuffer.Size = new System.Drawing.Size(144, 23);
			this.playPlasmaBuffer.TabIndex = 1;
			this.playPlasmaBuffer.Text = "play_prebuffered_plasma";
			this.playPlasmaBuffer.Click += new System.EventHandler(this.playPlasmaBuffer_Click);
			// 
			// sleepTimer
			// 
			this.sleepTimer.Location = new System.Drawing.Point(400, 568);
			this.sleepTimer.Maximum = new System.Decimal(new int[] {
																	   250,
																	   0,
																	   0,
																	   0});
			this.sleepTimer.Minimum = new System.Decimal(new int[] {
																	   1,
																	   0,
																	   0,
																	   0});
			this.sleepTimer.Name = "sleepTimer";
			this.sleepTimer.Size = new System.Drawing.Size(64, 20);
			this.sleepTimer.TabIndex = 3;
			this.sleepTimer.Value = new System.Decimal(new int[] {
																	 1,
																	 0,
																	 0,
																	 0});
			// 
			// plasmaColorCycle
			// 
			this.plasmaColorCycle.Location = new System.Drawing.Point(8, 632);
			this.plasmaColorCycle.Name = "plasmaColorCycle";
			this.plasmaColorCycle.Size = new System.Drawing.Size(112, 23);
			this.plasmaColorCycle.TabIndex = 1;
			this.plasmaColorCycle.Text = "plasma_color_cycle";
			this.plasmaColorCycle.Click += new System.EventHandler(this.plasmaColorCycle_Click);
			// 
			// plasmaArrayLockBits
			// 
			this.plasmaArrayLockBits.Location = new System.Drawing.Point(128, 632);
			this.plasmaArrayLockBits.Name = "plasmaArrayLockBits";
			this.plasmaArrayLockBits.Size = new System.Drawing.Size(152, 23);
			this.plasmaArrayLockBits.TabIndex = 1;
			this.plasmaArrayLockBits.Text = "plasma_array_LockBits()";
			this.plasmaArrayLockBits.Click += new System.EventHandler(this.plasmaArrayLockBits_Click);
			// 
			// plasmaFromJava
			// 
			this.plasmaFromJava.Location = new System.Drawing.Point(128, 600);
			this.plasmaFromJava.Name = "plasmaFromJava";
			this.plasmaFromJava.Size = new System.Drawing.Size(152, 23);
			this.plasmaFromJava.TabIndex = 1;
			this.plasmaFromJava.Text = "plasma_from_java";
			this.plasmaFromJava.Click += new System.EventHandler(this.plasmaFromJava_Click);
			// 
			// Plasma
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(616, 661);
			this.Controls.Add(this.sleepTimer);
			this.Controls.Add(this.start);
			this.Controls.Add(this.plasmaSimple);
			this.Controls.Add(this.image);
			this.Controls.Add(this.stop);
			this.Controls.Add(this.plasmaArray);
			this.Controls.Add(this.plasmaPreBuffer);
			this.Controls.Add(this.playPlasmaBuffer);
			this.Controls.Add(this.plasmaColorCycle);
			this.Controls.Add(this.plasmaArrayLockBits);
			this.Controls.Add(this.plasmaFromJava);
			this.Name = "Plasma";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.sleepTimer)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Plasma());
		}

		private void start_Click(object sender, System.EventArgs e)
		{
			running = true;
		}

		private void stop_Click(object sender, System.EventArgs e)
		{
			running = false;
			if (runningThread != null && runningThread.IsAlive)
			{
				runningThread.Abort();
			}
		}

		public static double ToRad(double angle)
		{
			return angle * (Math.PI / 180);
		}


		public static Color colorize(int i)
		{

			Color c;
			c = Color.FromArgb(Math.Abs(255 - i) % 255, Math.Abs(128 - i) % 255, Math.Abs(128 + i) % 255);
			/*
			if (i > 0)
			{
				c = Color.Black;
			}
			else
			{
				c = Color.White;
			}
			*/
			return c;
		}

		// TODO: write this with delegates instead, so that you select one, then start it with the button!
		private void plasmaSimple_Click(object sender, System.EventArgs e)
		{
			PlasmaSimple ps = new PlasmaSimple(width, height, image);
			Thread t = new Thread(new ThreadStart(ps.Run));
			runningThread = t;
			t.Start();
		}

		private void plasmaArray_Click(object sender, System.EventArgs e)
		{
			double [] sinArray = new double[360];
			// build sinus array (360 degree period)
			for (int i = 0; i < 360; i++)
			{
				sinArray[i] = Math.Sin(ToRad(i));
			}
			int inc = 0;
			double xValue;
			double yValue;
			Bitmap bmp = new Bitmap(width, height);
			Graphics g = image.CreateGraphics();
			while(running)
			{
				long start = System.DateTime.Now.Ticks;
				inc++;
				for (int x = 0; x < width; x++)
				{
					xValue =	20 * sinArray[((x * 4) + inc) % 360] + 
								30 * sinArray[(x + inc * 4) % 360] + 
								50 * sinArray[((x / 4) + (inc / 4)) % 360];
					for (int y = 0; y < height; y++)
					{
						yValue =	40 * sinArray[((y * 6) + inc) % 360] + 
									40 * sinArray[(y + inc * 6) % 360] + 
									20 * sinArray[(y + (inc / 6)) % 360];
						bmp.SetPixel(x,y, colorize(Convert.ToInt32(xValue + yValue)));
					}
				}
				//g.DrawImage(bmp,0,0);
				image.Image = bmp;
				image.Update();
				long timeToRenderOneFrame = System.DateTime.Now.Ticks - start;
				this.Text = "fps: " + 1 / (timeToRenderOneFrame * 0.0000001);
			}
		}

		private void plasmaPreBuffer_Click(object sender, System.EventArgs e)
		{
			double [] sinArray = new double[360];
			// build sinus array (360 degree period)
			for (int i = 0; i < 360; i++)
			{
				sinArray[i] = Math.Sin(ToRad(i));
			}

			int inc = 0;
			double xValue;
			double yValue;

			Bitmap [] plasma = new Bitmap[360];
			Graphics g = image.CreateGraphics();
			
			long start = System.DateTime.Now.Ticks;
			for (int t = 0; t < 360; t++)
			{
				Bitmap bmp = new Bitmap(width, height);
				inc++;
				for (int x = 0; x < width; x++)
				{
					xValue =	20 * sinArray[((x * 4) + inc) % 360] + 
						30 * sinArray[(x + inc * 4) % 360] + 
						50 * sinArray[((x / 4) + (inc / 4)) % 360];
					for (int y = 0; y < height; y++)
					{
						yValue =	40 * sinArray[((y * 6) + inc) % 360] + 
							40 * sinArray[(y + inc * 6) % 360] + 
							20 * sinArray[(y + (inc / 6)) % 360];
						bmp.SetPixel(x,y, colorize(Convert.ToInt32(xValue + yValue)));
					}
				}
				plasma[t] = bmp;
				double done = ((double)t / 360.0) * 100.0;
				this.Text = "Buffering: " + done + "%";
				this.Update();
			}
			long time = System.DateTime.Now.Ticks - start;
			this.Text = "Buffering took " + (time * 0.0000001) + " seconds";
			Console.Out.WriteLine("Buffering took " + (time * 0.0000001) + " seconds");
			IFormatter bf = new BinaryFormatter();
			Stream stream = new FileStream("plasma.bin", FileMode.OpenOrCreate);
			bf.Serialize(stream, plasma);
			stream.Close();
			


			
		}

		private void playPlasmaBuffer_Click(object sender, System.EventArgs e)
		{
			IFormatter bf = new BinaryFormatter();
			Stream stream = new FileStream("plasma.bin", FileMode.Open);
			Bitmap [] plasma = bf.Deserialize(stream) as Bitmap[];
			Graphics g = image.CreateGraphics();
			int p = 0;
			int sleep = (int)sleepTimer.Value;
			while(running)
			{
				p++;
				if (p > 359)
				{
					p = 0;
				}
				long start = System.DateTime.Now.Ticks;
				g.DrawImage(plasma[p],0,0);
				Thread.Sleep(sleep);
				long timeToRenderOneFrame = System.DateTime.Now.Ticks - start;
				this.Text = "fps: " + 1 / (timeToRenderOneFrame * 0.0000001) + ";p = " + p;
			}
		}
	
		struct PlasmaPoint
		{
			public double x;
			public double y;

			public PlasmaPoint(double x, double y)
			{
				this.x = x;
				this.y = y;
			}
		}	
		private void plasmaColorCycle_Click(object sender, System.EventArgs e)
		{
			double [] sinArray = new double[360];
			// build sinus array (360 degree period)
			for (int i = 0; i < 360; i++)
			{
				sinArray[i] = Math.Sin(ToRad(i));
			}
			int inc = 1; // change to move plasma?
			double xValue;
			double yValue;

			PlasmaPoint [,] plasma = new PlasmaPoint[width,height];
		
			Bitmap bmp = new Bitmap(width, height);
			Graphics g = image.CreateGraphics();
			for (int x = 0; x < width; x++)
			{
				xValue =	20 * sinArray[((x * 4) + inc) % 360] + 
					30 * sinArray[(x + inc * 4) % 360] + 
					50 * sinArray[((x / 4) + (inc / 4)) % 360];
				for (int y = 0; y < height; y++)
				{
					yValue =	40 * sinArray[((y * 6) + inc) % 360] + 
						40 * sinArray[(y + inc * 6) % 360] + 
						20 * sinArray[(y + (inc / 6)) % 360];
					plasma[x,y] = new PlasmaPoint(xValue, yValue);
				}
			}
			int t = 0;
			while (running)
			{
				long start = System.DateTime.Now.Ticks;
				t++;
				if (t > 255)
				{
					t = 0;
				}

				int x = 0;
				int y = 0;
				for (int n = 0; n < width*height; n++)
				{
					x = n % (width - 1);
					y = n / width;
					PlasmaPoint pp = plasma[x,y];
					// * and - work here aswell, instead of +
					bmp.SetPixel(x,y, colorize((int)(pp.x + pp.y) + t));
					
					
				}
				/*
				for (int x = 0; x < width; x++)
				{
					for (int y = 0; y < height; y++)
					{
						PlasmaPoint pp = plasma[x,y];
						bmp.SetPixel(x,y, colorize((int)(pp.x + pp.y) + t));
					}
				}
				*/
				g.DrawImage(bmp, 0,0);
				g.DrawString("Plasma!", new Font("verdana", (float)20.0),new SolidBrush(Color.White), 50, 50);
				long timeToRenderOneFrame = System.DateTime.Now.Ticks - start;
				this.Text = "fps: " + 1 / (timeToRenderOneFrame * 0.0000001);
			}
		}

		private void plasmaArrayLockBits_Click(object sender, System.EventArgs e)
		{
			PlasmaLockBits plb = new PlasmaLockBits(width, height, image);
			Thread t = new Thread(new ThreadStart(plb.Run));
			runningThread = t;
			t.Start();
		}

		private void plasmaFromJava_Click(object sender, System.EventArgs e)
		{
			PlasmaFromJava pfj = new PlasmaFromJava(width, height, image);
			Thread t = new Thread(new ThreadStart(pfj.Run));
			runningThread = t;
			t.Start();
		}
	}
}
