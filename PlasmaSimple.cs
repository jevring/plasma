using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace Plasma
{
	/// <summary>
	/// Summary description for PlasmaSimple.
	/// </summary>
	public class PlasmaSimple
	{
		double inc = 0.0d;
		int width;
		int height;
		Bitmap bmp;
		PictureBox image;
		bool running = true;

		public PlasmaSimple(int width, int height, PictureBox image)
		{
			this.width = width;
			this.height = height;
			this.image = image;
			bmp = new Bitmap(width, height);
		}

		public void Run()
		{
			double xValue;
			double yValue;
			while(running)
			{
				//long start = System.DateTime.Now.Ticks;
				inc++;
				for (int x = 0; x < width; x++)
				{
					xValue =	20 * Math.Sin(Plasma.ToRad((x * 4) + inc)) + 
						30 * Math.Sin(Plasma.ToRad(x + inc * 4)) + 
						50 * Math.Sin(Plasma.ToRad((x / 4) + (inc / 4)));
					for (int y = 0; y < height; y++)
					{
						yValue =	40 * Math.Sin(Plasma.ToRad((y * 6) + inc)) + 
							40 * Math.Sin(Plasma.ToRad(y + inc * 6)) + 
							20 * Math.Sin(Plasma.ToRad(y + (inc / 6)));
						bmp.SetPixel(x,y, Plasma.colorize(Convert.ToInt32(xValue + yValue)));
					}
				}
				image.Image = bmp;
				image.Update();
				//long timeToRenderOneFrame = System.DateTime.Now.Ticks - start;
				//this.Text = "fps: " + 1 / (timeToRenderOneFrame * 0.0000001);
				
			}
		}
	}
}
